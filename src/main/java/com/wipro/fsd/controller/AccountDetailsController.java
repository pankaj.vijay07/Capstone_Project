package com.wipro.fsd.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.wipro.fsd.bean.AccountDetailsBean;
import com.wipro.fsd.bean.CompleteDetailsBean;
import com.wipro.fsd.bean.CustomerDetailsBean;
import com.wipro.fsd.service.AccountDetailsService;
import com.wipro.fsd.service.CustomerDetailsService;
/* This is a controller class  which maps the account details restful request to its corresponding jsp pages and
helps to open,show detailsof the account by accountNumber or accountID
*/


@Controller
public class AccountDetailsController {


	@Value("${upload.path}")
	private String path;

	@Autowired
	AccountDetailsService accountService;

	@Autowired
	CustomerDetailsService customerService;

	// Open Account Page
	@RequestMapping("rest/openAccountPage")
	public String openAccount(HttpServletRequest req, HttpSession session,
			Model model) {

		if (session.getAttribute("user") != null)
			return "openAccount";
		else {
			model.addAttribute("secureMsg", "Please login first!");
			return "index";
		}
	}

	// Open Account
	@RequestMapping("rest/openAccount")
	public String createAccount(
			@ModelAttribute("details") CompleteDetailsBean details,
			Model model) {

		String flag = "failure";

		flag = accountService.newAccount(details);

		if (!flag.equals("failure")) {
			model.addAttribute("msg",
					"Account Opened Successfully.Account Number is: " + flag);
			flag = "adminHome";
		} else if (flag.equals("failure")) {
			model.addAttribute("msg", "Account is not opened.");
			flag = "adminHome";
		}
		return flag;
	}

	// View Details By AccountNumber
	@RequestMapping("rest/showDetailsByAccountNumber")
	public String searchProfile(
			@RequestParam("showDetailsByAccountNumber") String accountNumber,
			Model model) {

		int cust_id = 0;
		String flag = "errors";
		List<CustomerDetailsBean> customerBean = null;
		List<AccountDetailsBean> accountBean = null;

		accountBean = accountService
				.viewAccountDetailsByAccountNumber(accountNumber);

		if (accountBean.size() > 0)
			cust_id = accountBean.get(0).getCustomerDetailsBean()
					.getCustomerId();

		customerBean = customerService.viewAccountByAccountNumber(cust_id);

		if (accountBean != null && customerBean != null) {
			model.addAttribute("msg", "Details!");
			model.addAttribute("accountlist", accountBean);
			model.addAttribute("customerlist", customerBean);
			flag = "showCustomer";
		} else if (accountBean == null || customerBean == null) {
			model.addAttribute("msg", "Job is not Executed.");
			flag = "showCustomer";
		}
		return flag;
	}

	// Show Account Details Account
	@RequestMapping("rest/AccountInfoPage")
	public String AccountInfo(HttpSession session, Model model) {

		String flag = "failure";
		List<AccountDetailsBean> accountBeans = null;
		int cust_id = (Integer) session.getAttribute("user");
		accountBeans = (List<AccountDetailsBean>) accountService
				.viewAccountDetailsByCustomerId(cust_id + "");

		if (accountBeans != null) {
			AccountDetailsBean accountBean = accountBeans.get(0);
			model.addAttribute("accountBeans", accountBeans);
			flag = "showAccountInfo";
		} else if (flag.equals("failure")) {
			model.addAttribute("msg", "Account is not opened.");
			flag = "userHome";
		}
		return flag;
	}

	}
