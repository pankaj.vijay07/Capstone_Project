package com.wipro.fsd.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wipro.fsd.bean.AccountDetailsBean;
import com.wipro.fsd.bean.UserDetailsBean;
import com.wipro.fsd.service.AccountDetailsService;
import com.wipro.fsd.service.UserDetailsService;
/* This is a controller class  which maps the user details restful request to its corresponding jsp pages and
helps to register,login and change the password of the user.
*/



@Controller
public class UserDetailsController {

	@Autowired
	UserDetailsService userDetailsService;

	@Autowired
	AccountDetailsService accountDetailsService;

	// Index page
	@RequestMapping("/rest/home")
	public String home() {

		return "index";
	}

	// SignUp page
	@RequestMapping("/rest/signUp")
	public String signUpPage() {

		return "signUp";
	}

	// Registration
	@RequestMapping("/rest/register")
	public String signUp(@ModelAttribute("Emp") UserDetailsBean emp,
			Model model) {

		String result = "failure";
		String flag = null;
		flag = userDetailsService.save(emp);

		if (emp.getUserType().equalsIgnoreCase("admin") && flag != "failure") {
			model.addAttribute("msg",
					"Registration Successful with UserId 'xxxx'");
			model.addAttribute("msg2", "Please contact to admin to get UserId");
			result = "success";
		} else
			if (emp.getUserType().equalsIgnoreCase("user")
					&& flag != "failure") {
			model.addAttribute("msg",
					"Registration Successful with UserId " + flag);
			model.addAttribute("msg2", "Please login with UserId " + flag);
			result = "success";
		}

		return result;
	}

	// LogOut Page
	@RequestMapping("rest/logOutPage")
	public String adminLogOut(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		session.invalidate();
		model.addAttribute("msg", "You are logged out now!");
		return "index";
	}

	// Change Admin Password Page
	@RequestMapping("rest/changePasswordPage")
	public String adminPasswordChange(HttpServletRequest req,
			HttpSession session, Model model) {

		if (session.getAttribute("user") != null)
			return "changeAdminPassword";
		else {
			model.addAttribute("secureMsg", "Please login first!");
			return "index";
		}
	}

	// Change Admin Password
	@RequestMapping("rest/changeMyPassword")
	public String alterPassword(@PathParam("newPassword") String newPassword,
			HttpServletRequest req, HttpSession session, Model model) {

		String flag = "failure";
		int userId = (Integer) session.getAttribute("user");
		System.out.println("UserId:" + userId);
		flag = userDetailsService.changePassword(userId, newPassword);

		if (!flag.equals("failure")) {
			model.addAttribute("msg", "Password Changed Successfully.");
			flag = "adminHome";
		} else if (flag.equals("failure")) {
			model.addAttribute("msg",
					"Something went wrong! Please try again.");
			flag = "adminHome";
		}
		return flag;
	}

	// Change User Password Page
	@RequestMapping("rest/changeUserPasswordPage")
	public String userPasswordChange(HttpServletRequest req,
			HttpSession session, Model model) {

		if (session.getAttribute("user") != null)
			return "changeUserPassword";
		else {
			model.addAttribute("secureMsg", "Please login first!");
			return "index";
		}
	}

	// Change User Password
	@RequestMapping("rest/changeUserPassword")
	public String alterUserPassword(
			@PathParam("newPassword") String newPassword,
			HttpServletRequest req, HttpSession session, Model model) {

		String flag = "failure";
		int userId = (Integer) session.getAttribute("user");
		System.out.println("UserId:" + userId);
		flag = userDetailsService.changePassword(userId, newPassword);

		if (!flag.equals("failure")) {
			model.addAttribute("msg", "Password Changed Successfully.");
			flag = "userHome";
		} else if (flag.equals("failure")) {
			model.addAttribute("msg",
					"Something went wrong! Password is not changed.");
			flag = "userHome";
		}
		return flag;
	}

	// Employee Login
	@PostMapping("/rest/login")
	public String register(@ModelAttribute("Emp") UserDetailsBean user,
			Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {

		List<AccountDetailsBean> accountBeans = null;
		UserDetailsBean userDB = null;
		String flag = "error";
		System.out.println("session " + session);
		System.out.println(user);
		userDB = userDetailsService.login(user);
		System.out.println(userDB);
		if (userDB != null && userDB.getPassword().equals(user.getPassword())
				&& userDB.getUserType().equalsIgnoreCase("admin")) {
			System.out.println("Welcome Admin!");
			flag = "adminHome";
			model.addAttribute("msg", "welcome Admin: " + userDB.getUserName());

			session = request.getSession();
			session.setAttribute("user", userDB.getUserId());
			session.setAttribute("pass", userDB.getPassword());
		} else
			if (userDB != null
					&& userDB.getPassword().equals(user.getPassword())
					&& userDB.getUserType().equalsIgnoreCase("user")) {
			System.out.println("Welcome User!");
			accountBeans = (List<AccountDetailsBean>) accountDetailsService
					.viewAccountDetailsByCustomerId(userDB.getUserId() + "");

			if (accountBeans != null) {
				AccountDetailsBean accountBean = accountBeans.get(0);
				model.addAttribute("accountBeans", accountBeans);
			}
			model.addAttribute("msg", "welcome User: " + userDB.getUserName());
			flag = "userHome";

			session = request.getSession();
			session.setAttribute("user", userDB.getUserId());
			session.setAttribute("pass", userDB.getPassword());
		} else {
			model.addAttribute("msg", "Login failed!");
			flag = "index";
		}
		return flag;
	}

}
