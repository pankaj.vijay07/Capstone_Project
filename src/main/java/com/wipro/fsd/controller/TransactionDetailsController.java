package com.wipro.fsd.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wipro.fsd.bean.TransactionDetailsBean;
import com.wipro.fsd.service.AccountDetailsService;
import com.wipro.fsd.service.CustomerDetailsService;
import com.wipro.fsd.service.TransactionDetailsService;
/* This is a controller class  which maps the transaction details restful request to its corresponding jsp pages and
helps to display the transaction history of your account by applying filters such as by date or by fundReceivedDate.
*/

@Controller
public class TransactionDetailsController {

	@Autowired
	TransactionDetailsService transactionDetailsService;

	@Autowired
	AccountDetailsService accountDetailsService;

	@Autowired
	CustomerDetailsService customerDetailsService;

	// Open fund transfer Page
	@RequestMapping("rest/fundTransferPage")
	public String fundTransferPage(HttpServletRequest req, HttpSession session,
			Model model) {

		if (session.getAttribute("user") != null)
			return "fundTransfer";
		else {
			model.addAttribute("secureMsg", "Please login first!");
			return "index";
		}
	}

	// Open Account
	@RequestMapping("rest/fundTransfer")
	public String fundTransfer(
			@PathParam("senderAccountNumber") String senderAccountNumber,
			@PathParam("beneficieryAccountNumber") String beneficieryAccountNumber,
			@PathParam("beneficieryName") String beneficieryName,
			@PathParam("bankName") String bankName,
			@PathParam("accoutType") String accoutType,
			@PathParam("amount") String amount,
			@PathParam("phone") String phone,HttpSession session, Model model) {

		String flag = "failure";

		int userId = (Integer) session.getAttribute("user");

		// for validating account information.
		flag = transactionDetailsService.infoValidation(userId,
				beneficieryAccountNumber, beneficieryName, bankName, accoutType,
				amount);

		if (flag.equals("Insufficient Amount!")) {
			model.addAttribute("msg",
					"Transaction failed due to insufficient balance.");
			flag = "userHome";
		} else if (flag.equals("Beneficiery Account Number is not Valid!")) {
			model.addAttribute("msg",
					"Transaction failed due to incorrect beneficiery account number.");
			flag = "userHome";
		} else if (flag.equals("Beneficiery Bank name is not Valid!")) {
			model.addAttribute("msg",
					"Transaction failed due to incorrect beneficiery bank name.");
			flag = "userHome";
		} else if (flag.equals("Beneficiery Account Type is not Valid!")) {
			model.addAttribute("msg",
					"Transaction failed due to incorrect beneficiery Bank Type.");
			flag = "userHome";
		} else if (flag.equals("success")) {
			model.addAttribute("msg", "Fund transfered Successfully.");
			flag = "userHome";
		} else if (flag.equals("failure")) {
			model.addAttribute("msg", "Account is not opened.");
			flag = "userHome";
		}
		return flag;
	}

	// Open fund transfer Page
	@RequestMapping("rest/transactionHistoryPage")
	public String transactionHistoryPage(HttpServletRequest req,
			HttpSession session, Model model) {

		if (session.getAttribute("user") != null)
			return "transactionHistory";
		else {
			model.addAttribute("secureMsg", "Please login first!");
			return "index";
		}
	}

	// Fund Transfered Transaction History
	@RequestMapping("rest/fundTransferedTransactionHistory")
	public String transactionHistory(
			@PathParam("senderAccountNumber") String senderAccountNumber,
			@PathParam("beneficieryAccountNumber") String beneficieryAccountNumber,
			@PathParam("beneficieryName") String beneficieryName,
			@PathParam("bankName") String bankName,
			@PathParam("accoutType") String accoutType,
			@PathParam("amount") String amount,
			@PathParam("phone") String phone,HttpSession session, Model model) {

		String flag = "failure";

		int userId = (Integer) session.getAttribute("user");

		// for validating account information.
		flag = transactionDetailsService.infoValidation(userId,
				beneficieryAccountNumber, beneficieryName, bankName, accoutType,
				amount);

		if (flag.equals("Insufficient Amount!")) {
			model.addAttribute("msg",
					"Transaction failed due to insufficient balance.");
			flag = "userHome";
		} else if (flag.equals("Beneficiery Account Number is not Valid!")) {
			model.addAttribute("msg",
					"Transaction failed due to incorrect beneficiery account number.");
			flag = "userHome";
		} else if (flag.equals("Beneficiery Bank name is not Valid!")) {
			model.addAttribute("msg",
					"Transaction failed due to incorrect beneficiery bank name.");
			flag = "userHome";
		} else if (flag.equals("Beneficiery Account Type is not Valid!")) {
			model.addAttribute("msg",
					"Transaction failed due to incorrect beneficiery Bank Type.");
			flag = "userHome";
		} else if (flag.equals("success")) {
			model.addAttribute("msg", "Fund transfered Successfully.");
			flag = "userHome";
		} else if (flag.equals("failure")) {
			model.addAttribute("msg", "Account is not opened.");
			flag = "userHome";
		}
		return flag;
	}

	// Fund Transfered History
	@RequestMapping("rest/fundTransferedHistory")
	public String fundTransferedHistory(HttpSession session, Model model) {

		String flag = "errors";
		List<TransactionDetailsBean> list = null;
		int userId = (Integer) session.getAttribute("user");

		System.out.println(userId);
		// for fetching information
		list = transactionDetailsService.fundTransferedHistory(userId);

		for (TransactionDetailsBean h : list) {
			System.out.println(h);
		}
		if (list != null) {
			model.addAttribute("msg", "Transaction History");
			model.addAttribute("list", list);
			flag = "transactionHistory";
		}

		else if (list == null) {
			model.addAttribute("msg", "History can't be shown.");
			flag = "transactionHistory";
		}
		return flag;
	}

	// Fund Received History
	@RequestMapping("rest/fundReceivedHistory")
	public String fundReceivedHistory(HttpSession session, Model model) {

		String flag = "errors";
		List<TransactionDetailsBean> list = null;
		int userId = (Integer) session.getAttribute("user");

		System.out.println(userId);
		// for fetching information
		list = transactionDetailsService.fundReceivedHistory(userId);

		for (TransactionDetailsBean h : list) {
			System.out.println(h);
		}
		if (list != null) {
			model.addAttribute("msg", "Transaction History");
			model.addAttribute("list", list);
			flag = "transactionHistory";
		}

		else if (list == null) {
			model.addAttribute("msg", "History can't be shown.");
			flag = "transactionHistory";
		}
		return flag;
	}

	// Transactions History By Date
	@RequestMapping("rest/fundHistoryByDate")
	public String fundHistoryByDate(@PathParam("fromDate") String fromDate,
			@PathParam("toDate") String toDate, HttpSession session,
			Model model) {

		System.out.println(fromDate);
		String flag = "errors";
		List<TransactionDetailsBean> list = null;
		int userId = (Integer) session.getAttribute("user");

		System.out.println(userId);
		// for fetching information
		list = transactionDetailsService.fundHistoryByDateforUser(userId, fromDate,
				toDate);

		for (TransactionDetailsBean h : list) {
			System.out.println(h);
		}
		if (list != null) {
			model.addAttribute("msg", "Transaction History");
			model.addAttribute("list", list);
			flag = "transactionHistory";
		}

		else if (list == null) {
			model.addAttribute("msg", "History can't be shown.");
			flag = "transactionHistory";
		}
		return flag;
	}

	// TxHistory Admin Page
	@RequestMapping("rest/transactionHistoryAdminPage")
	public String transactionHistoryAdminPage(HttpServletRequest req,
			HttpSession session, Model model) {

		if (session.getAttribute("user") != null)
			return "transactionsHistory";
		else {
			model.addAttribute("secureMsg", "Please login first!");
			return "index";
		}
	}

	// Transactions History By Date 2
	@RequestMapping("rest/transactionsHistoryByDate2")
	public String fundHistoryByDate2(@PathParam("customerId") String customerId,
			@PathParam("fromDate") String fromDate,
			@PathParam("toDate") String toDate, Model model) {

		String flag = "errors";
		List<TransactionDetailsBean> list = null;
		int userId = Integer.parseInt(customerId);

		System.out.println(userId);
		// for fetching information
		list = transactionDetailsService.fundHistoryByDate(userId, fromDate, toDate);

		for (TransactionDetailsBean h : list) {
			System.out.println(h);
		}
		if (list != null) {
			model.addAttribute("msg", "Transaction History");
			model.addAttribute("list", list);
			flag = "transactionsHistory";
		}

		else if (list == null) {
			model.addAttribute("msg", "History can't be shown.");
			flag = "transactionsHistory";
		}
		return flag;
	}

	// Transactions History By Date 2
	@RequestMapping("rest/transactionsHistoryByTxID")
	public String HistoryByTransactionID(@PathParam("txID") String txID,
			Model model) {

		String flag = "errors";
		List<TransactionDetailsBean> list = new ArrayList<TransactionDetailsBean>();
		TransactionDetailsBean bean = null;
		bean = transactionDetailsService.TxHistoryByTransactionID(txID);

		if (bean != null) {
			list.add(bean);
			model.addAttribute("msg", "Transaction History");
			model.addAttribute("list", list);
			flag = "transactionsHistory";
		}

		else if (bean == null) {
			model.addAttribute("msg", "History can't be shown.");
			flag = "transactionsHistory";
		}
		return flag;
	}

}
