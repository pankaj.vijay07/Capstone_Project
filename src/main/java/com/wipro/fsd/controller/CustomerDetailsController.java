package com.wipro.fsd.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.wipro.fsd.bean.AccountDetailsBean;
import com.wipro.fsd.bean.CustomerDetailsBean;
import com.wipro.fsd.service.AccountDetailsService;
import com.wipro.fsd.service.CustomerDetailsService;
/* This is a controller class  which maps the customer details restful request to its corresponding jsp pages and
helps to update,show detailsof the account by accountNumber or accountID and delete the customer from the database
*/



@Controller
public class CustomerDetailsController {

	@Autowired
	CustomerDetailsService customerService;

	@Autowired
	AccountDetailsService accountService;

	// Show Customer & Account Details Page
	@RequestMapping("rest/showCustomerPage")
	public String custaccountDetailsPage(HttpServletRequest req, HttpSession session,
			Model model) {

		if (session.getAttribute("user") != null)
			return "showCustomer";
		else {
			model.addAttribute("secureMsg", "Please login first!");
			return "index";
		}
	}

	// View Details By CustomerId
	@RequestMapping("rest/showDetailsByCustomerId")
	public String searchProfile(
			@RequestParam("showDetailsByCustomerId") String customerId,
			Model model) {

		String flag = "errors";
		List<CustomerDetailsBean> customerlist = null;
		List<AccountDetailsBean> accountlist = null;

		accountlist = accountService.viewAccountDetailsByCustomerId(customerId);
		customerlist = customerService.viewAccountByCustomerId(customerId);

		for (AccountDetailsBean a : accountlist)
			System.out.println("Account Bean:" + a.getAccountNumber());

		if (accountlist != null && customerlist != null) {
			model.addAttribute("msg", "Details!");
			model.addAttribute("accountlist", accountlist);
			model.addAttribute("customerlist", customerlist);
			flag = "showCustomer";
		} else if (accountlist == null) {
			model.addAttribute("msg", "Job is not Executed.");
			flag = "showCustomer";
		}
		return flag;
	}

	// Update Customer Details Page
	@RequestMapping("rest/updateCustomerPage")
	public String updateCustomerPage(HttpServletRequest req,
			HttpSession session, Model model) {

		if (session.getAttribute("user") != null)
			return "updateCustomer";
		else {
			model.addAttribute("secureMsg", "Please login first!");
			return "index";
		}
	}

	/*
	 * // Modify Customer Email
	 * 
	 * @RequestMapping("rest/updateCustomerEmail") public String
	 * updateEmail(@RequestParam("updateCustomerId") String updateCustomerId,
	 * 
	 * @RequestParam("updateCustomerEmail") String updateCustomerEmail, Model
	 * model) {
	 * 
	 * String flag="errors";
	 * flag=customerService.updateCustomerEmail(updateCustomerId,
	 * updateCustomerEmail);
	 * 
	 * if(flag.equals("success")) { model.addAttribute("msg",
	 * "Email Updated Successfully!"); flag="adminHome"; } else
	 * if(flag.equals("errors")) { model.addAttribute("msg",
	 * "Email is not updated."); flag="adminHome"; } return flag; }
	 */

	// Modify Customer Address
	@RequestMapping("rest/updateCustomerAddress")
	public String updateAddress(
			@RequestParam("updateCustomerId") String customerId,
			@RequestParam("updateCustomerAddress") String customerAddress,
			Model model) {

		String flag = "errors";
		flag = customerService.updateCustomerAddress(customerId,
				customerAddress);

		if (flag.equals("success")) {
			model.addAttribute("msg", "Address Updated Successfully!");
			flag = "adminHome";
		} else if (flag.equals("errors")) {
			model.addAttribute("msg", "Address is not updated.");
			flag = "adminHome";
		}
		return flag;
	}

	// Delete Customer Page
	@RequestMapping("rest/deleteCustomerPage")
	public String deleteCustomerPage(HttpServletRequest req,
			HttpSession session, Model model) {

		if (session.getAttribute("user") != null)
			return "deleteCustomer";
		else {
			model.addAttribute("secureMsg", "Please login first!");
			return "index";
		}
	}

	// Delete Customer
	@RequestMapping("rest/deleteCustomer")
	public String deleteCustomer(@RequestParam("customerId") String customerId,
			Model model) {

		String flag = "errors";
		flag = customerService.deleteCustomer(customerId);

		if (flag.equals("success")) {
			model.addAttribute("msg", "Customer Deleted Successfully!");
			flag = "adminHome";
		} else if (flag.equals("errors")) {
			model.addAttribute("msg", "Customer is not deleted.");
			flag = "adminHome";
		}
		return flag;
	}

	// View Personal Details By CustomerId
	@RequestMapping("rest/personalDetailsPage")
	public String viewPersonalDetails(HttpSession session, Model model) {

		String flag = "errors";
		List<CustomerDetailsBean> customerlist = new ArrayList<CustomerDetailsBean>();

		int cust_id = (Integer) session.getAttribute("user");
		System.out.println("Customer Id: " + cust_id);
		CustomerDetailsBean customer = customerService
				.customerPersonalDetails(cust_id + "");

		if (customer != null) {
			customerlist.add(customer);
			model.addAttribute("msg", "Details!");
			model.addAttribute("customerlist", customerlist);
			flag = "customerPersonalDetails";
		} else if (customer == null) {
			model.addAttribute("msg", "Can't Display Data.");
			flag = "customerPersonalDetails";
		}
		return flag;
	}

	// Update Customer Details Page
	@RequestMapping("rest/updateCustomerDetailsPage")
	public String updateCustomerDetailsPage(HttpServletRequest req,
			HttpSession session, Model model) {

		if (session.getAttribute("user") != null)
			return "updateDetails";
		else {
			model.addAttribute("secureMsg", "Please login first!");
			return "index";
		}
	}

	/*
	 * // Modify Customer Email (By Customer)
	 * 
	 * @RequestMapping("rest/updateEmailByCustomer") public String
	 * updateEmailByCustomer(@RequestParam("updateCustomerEmail") String
	 * updateCustomerEmail, Model model, HttpSession session) {
	 * 
	 * String flag="errors"; int cust_id=(Integer) session.getAttribute("user");
	 * flag=customerService.updateCustomerEmail(cust_id+"",updateCustomerEmail);
	 * 
	 * if(flag.equals("success")) { model.addAttribute("msg",
	 * "Email Updated Successfully!"); flag="userHome"; } else
	 * if(flag.equals("errors")) { model.addAttribute("msg",
	 * "Email is not updated."); flag="userHome"; } return flag; }
	 */
	// Modify Customer Address
	@RequestMapping("rest/updateAddressByCustomer")
	public String updateAddressByCustomer(HttpSession session,
			@RequestParam("updateCustomerAddress") String customerAddress,
			Model model) {

		String flag = "errors";
		int cust_id = (Integer) session.getAttribute("user");
		flag = customerService.updateCustomerAddress(cust_id + "",
				customerAddress);

		if (flag.equals("success")) {
			model.addAttribute("msg", "Address Updated Successfully!");
			flag = "userHome";
		} else if (flag.equals("errors")) {
			model.addAttribute("msg", "Address is not updated.");
			flag = "userHome";
		}
		return flag;
	}

}
