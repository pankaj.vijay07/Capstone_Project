package com.wipro.fsd.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/*
 * Case study 1: Account Management System Monolithic Java FSD
1. Customer can have one or more bank accounts (Eg: ICICI,HDFC..).
2. Transfer amount from one account to another account
3. Create CRUD operations for Customer and Accounts 
4. Develop customer login , after successful login show accounts summary. Click on account display account information available balance.
5. Transactions Filter/Search operation from date - to date show Transaction summary
Maintain all data in one DB
*/

@ComponentScan(basePackages="com.wipro.fsd")
@SpringBootApplication
public class SpringBootWebApplication extends SpringBootServletInitializer {

	
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringBootWebApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringBootWebApplication.class, args);
	}
	
	
	
}
