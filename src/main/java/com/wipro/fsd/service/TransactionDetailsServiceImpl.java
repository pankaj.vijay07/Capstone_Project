package com.wipro.fsd.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wipro.fsd.bean.TransactionDetailsBean;
import com.wipro.fsd.dao.TransactionDetailsDAO;


@Component
@Transactional
public class TransactionDetailsServiceImpl implements TransactionDetailsService{

	
	@Autowired
	TransactionDetailsDAO transactionDetailsDAO;
	
	
	@Override
	public String infoValidation(int userId, String beneficieryAccountNumber,
			String beneficieryName, String bankName, String accoutType,
			String amount) {
		// TODO Auto-generated method stub

		
		return transactionDetailsDAO.infoValidation(userId, beneficieryAccountNumber, beneficieryName, bankName, accoutType, amount);
	
	
	}

	@Override
	public List<TransactionDetailsBean> fundTransferedHistory(int userId) {
		// TODO Auto-generated method stub

	return transactionDetailsDAO.fundTransferedHistory(userId);
	}

	@Override
	public List<TransactionDetailsBean> fundReceivedHistory(int userId) {
		// TODO Auto-generated method stub
return transactionDetailsDAO.fundReceivedHistory(userId);
	
	
	}

	@Override
	public List<TransactionDetailsBean> fundHistoryByDate(int userId,
			String fromDate, String toDate) {
		// TODO Auto-generated method stub

	return transactionDetailsDAO.fundHistoryByDate(userId, fromDate, toDate);
	}

	@Override
	public TransactionDetailsBean TxHistoryByTransactionID(String txID) {
		// TODO Auto-generated method stub

	return transactionDetailsDAO.TxHistoryByTransactionID(txID);
	}

	@Override
	public List<TransactionDetailsBean> fundHistoryByDateforUser(int userId,
			String fromDate, String toDate) {
		// TODO Auto-generated method stub

	return transactionDetailsDAO.fundHistoryByDateforUser(userId, fromDate, toDate);
	}

}
