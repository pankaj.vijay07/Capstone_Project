package com.wipro.fsd.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wipro.fsd.bean.UserDetailsBean;
import com.wipro.fsd.dao.UserDetailsDAO;


@Component
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserDetailsDAO userDAO;
	
	
	@Override
	public String save(UserDetailsBean emp) {
		// TODO Auto-generated method stub

	String flag="failure";
	
	flag=userDAO.save(emp);
	
	return flag;
	
	}

	@Override
	public UserDetailsBean login(UserDetailsBean user) {
		// TODO Auto-generated method stub

	return userDAO.login(user);
	
	
	}

	@Override
	public String changePassword(int userId, String newPassword) {
		// TODO Auto-generated method stub

	return userDAO.changePassword(userId, newPassword);
	
	}

}
