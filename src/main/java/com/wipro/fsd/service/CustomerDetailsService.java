package com.wipro.fsd.service;

import java.util.List;

import com.wipro.fsd.bean.CustomerDetailsBean;

public interface CustomerDetailsService {

	List<CustomerDetailsBean> viewAccountByCustomerId(String customerId);

	List<CustomerDetailsBean> viewAccountByAccountNumber(int id);


	String updateCustomerAddress(String customerId, String customerAddress);

	String deleteCustomer(String customerId);

	CustomerDetailsBean customerPersonalDetails(String cust_id);
	
}
