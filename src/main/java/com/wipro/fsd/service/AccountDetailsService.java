package com.wipro.fsd.service;

import java.util.List;

import com.wipro.fsd.bean.AccountDetailsBean;
import com.wipro.fsd.bean.CompleteDetailsBean;

public interface AccountDetailsService {

	String newAccount(CompleteDetailsBean complete_details);

	List<AccountDetailsBean> viewAccountDetailsByCustomerId(String customerId);

	List<AccountDetailsBean> viewAccountDetailsByAccountNumber(String accountNumber);
	
	
	
}
