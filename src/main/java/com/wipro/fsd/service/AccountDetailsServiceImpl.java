package com.wipro.fsd.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wipro.fsd.bean.AccountDetailsBean;
import com.wipro.fsd.bean.CompleteDetailsBean;
import com.wipro.fsd.dao.AccountDetailsDAO;



@Component
@Transactional
public class AccountDetailsServiceImpl implements AccountDetailsService {

	@Autowired
	AccountDetailsDAO accountDetailsDAO;

	@Override
	public String newAccount(CompleteDetailsBean complete_details) {
		// TODO Auto-generated method stub

		String flag = "failure";

		flag = accountDetailsDAO.saveAccount(complete_details);

		return flag;

	}

	@Override
	public List<AccountDetailsBean> viewAccountDetailsByCustomerId(
			String customerId) {
		// TODO Auto-generated method stub
		List<AccountDetailsBean> accountBean = null;

		accountBean = accountDetailsDAO
				.viewAccountDetailsByCustomerId(customerId);

		return accountBean;

	}

	@Override
	public List<AccountDetailsBean> viewAccountDetailsByAccountNumber(
			String accountNumber) {
		// TODO Auto-generated method stub

		List<AccountDetailsBean> accountBean = null;

		accountBean = viewAccountDetailsByAccountNumber(accountNumber);
		return accountBean;

	}

}
