package com.wipro.fsd.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wipro.fsd.bean.CustomerDetailsBean;
import com.wipro.fsd.dao.CustomerDetailsDAO;


@Component
@Transactional
public class CustomerDetailsServiceImpl implements CustomerDetailsService {

	@Autowired
	CustomerDetailsDAO customerDetailsDAO;

	@Override
	public List<CustomerDetailsBean> viewAccountByCustomerId(
			String customerId) {
		// TODO Auto-generated method stub

		List<CustomerDetailsBean> customerBean = null;

		customerBean = customerDetailsDAO.viewAccountByCustomerId(customerId);

		return customerBean;

	}

	@Override
	public List<CustomerDetailsBean> viewAccountByAccountNumber(int id) {
		// TODO Auto-generated method stub

		List<CustomerDetailsBean> customerBean = null;

		customerBean = customerDetailsDAO.viewAccountByAccountNumber(id);
		return customerBean;

	}

	@Override
	public String updateCustomerAddress(String customerId,
			String customerAddress) {
		// TODO Auto-generated method stub

		String flag = customerDetailsDAO.updateCustomerAddress(customerId,
				customerAddress);

		return flag;

	}

	@Override
	public String deleteCustomer(String customerId) {
		// TODO Auto-generated method stub

		String flag = customerDetailsDAO.deleteCustomer(customerId);

		return flag;

	}

	@Override
	public CustomerDetailsBean customerPersonalDetails(String cust_id) {
		// TODO Auto-generated method stub

		CustomerDetailsBean customerBean = customerDetailsDAO
				.customerPersonalDetails(cust_id);

		return customerBean;

	}

}
