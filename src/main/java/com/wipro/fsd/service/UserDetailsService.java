package com.wipro.fsd.service;

import com.wipro.fsd.bean.UserDetailsBean;

public interface UserDetailsService {

	
	public String save(UserDetailsBean emp);

	public UserDetailsBean login(UserDetailsBean user);

	public String changePassword(int userId,String newPassword);
	
	
}
