package com.wipro.fsd.service;

import java.util.List;

import com.wipro.fsd.bean.TransactionDetailsBean;

public interface TransactionDetailsService {

	String infoValidation(int userId, String beneficieryAccountNumber,
			String beneficieryName, String bankName, String accoutType,
			String amount);

	List<TransactionDetailsBean> fundTransferedHistory(int userId);

	List<TransactionDetailsBean> fundReceivedHistory(int userId);

	List<TransactionDetailsBean> fundHistoryByDate(int userId, String fromDate, String toDate);

	TransactionDetailsBean TxHistoryByTransactionID(String txID);

	List<TransactionDetailsBean> fundHistoryByDateforUser(int userId, String fromDate,
			String toDate);

	
	
	
	
	
}
