package com.wipro.fsd.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wipro.fsd.bean.AccountDetailsBean;
import com.wipro.fsd.bean.TransactionDetailsBean;

@Repository
public class TransactionDetailsDAOImpl implements TransactionDetailsDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	// Bank Information Validation

	public String infoValidation(int userId,String beneficieryAccountNumber,
			String beneficieryName, String bankName, String accountType,
			String amount) {
		
		String isValid="";
		int accountBalanceOfSender=0;
		int accountBalanceOfReceiver=0;
		try
		{
	    System.out.println("Fetching record.....................");
		Session session=sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("from AccountDetailsBean where customerBean.customerId = :id");
		query.setParameter("id",userId);
		List<AccountDetailsBean> list=query.list();
		AccountDetailsBean senderAccount=list.get(0);
		accountBalanceOfSender=Integer.parseInt(senderAccount.getAccountBalance());
		if(accountBalanceOfSender < Integer.parseInt(amount))
		{
			return "Insufficient Amount!";
		}
		
		AccountDetailsBean beneficieryAccount=session.get(AccountDetailsBean.class,Integer.parseInt(beneficieryAccountNumber));
		if(beneficieryAccount==null)
		{
			return "Beneficiery Account Number is not Valid!";
		}
		if(beneficieryAccount!=null)
		{
			accountBalanceOfReceiver=Integer.parseInt(beneficieryAccount.getAccountBalance());
		}
		if(!beneficieryAccount.getBankName().trim().equalsIgnoreCase(bankName.trim()))
		{
			return "Beneficiery Bank name is not Valid!";
		}
		
		if(!beneficieryAccount.getAccountType().equalsIgnoreCase(accountType))
		{
			return "Beneficiery Account Type is not Valid!";
		}
		
		int updatedBalanceForSender=accountBalanceOfSender-Integer.parseInt(amount);
		int updatedBalanceForReceiver=accountBalanceOfReceiver+Integer.parseInt(amount);
		senderAccount.setAccountBalance(updatedBalanceForSender+"");
		beneficieryAccount.setAccountBalance(updatedBalanceForReceiver+"");
		
		session.update(senderAccount);
		session.update(beneficieryAccount);
		
		// Creating Transaction Id
		String transactionId=getTransactionId();
		
		// Saving Transaction Details
		TransactionDetailsBean bean= saveTransactionDetails(transactionId,senderAccount.getAccountNumber(),
													beneficieryAccount.getAccountNumber(),
													accountType,bankName,amount);
		
		// saving Transaction details
		session.save(bean);
		isValid="success";
		}
		catch(Exception e)
		{
			isValid="failure";
			e.printStackTrace();
		}
		return isValid;
	}
	
	private TransactionDetailsBean saveTransactionDetails(String transactionId,
			int accountNumber, int beneficieryAccount,
			String accoutType, String bankName, String amount) {
		
		TransactionDetailsBean bean=new TransactionDetailsBean();
		bean.setTransactionId(transactionId);
		bean.setFromAccount(accountNumber);
		bean.setToAccount(beneficieryAccount);
		bean.setAccountType(accoutType);
		bean.setTransferredAmount(amount);
		bean.setDateOfTransaction(new Date());
		
		return bean;
			
	}
	private String getTransactionId() {
		
		int id=0;
		Random r=new Random();
		id=r.nextInt(10000)*r.nextInt(10000)+r.nextInt(10000)*r.nextInt(10000)
				+r.nextInt(10000)*r.nextInt(10000)+r.nextInt(10000)*r.nextInt(10000);
		
		return "TX00"+id;
	}
	

	public List<TransactionDetailsBean> fundTransferedHistory(int userId) {
		
		List<TransactionDetailsBean> list=null;
		List<AccountDetailsBean> AccountDetailsBeans=null;
		AccountDetailsBean AccountDetailsBean=null;
		try
		{
	    System.out.println("Fetching record.....................");
		Session session=sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("from AccountDetailsBean where customerBean.customerId = :id");
		query.setParameter("id",userId);
		AccountDetailsBeans=query.list();
		
		if(AccountDetailsBeans!=null){
		for(AccountDetailsBean a:AccountDetailsBeans)
		{
			System.out.println(a);
		}
		AccountDetailsBean=AccountDetailsBeans.get(0);
		}
		
		System.out.println(AccountDetailsBean);
		
		Query query2 = session.createQuery("from TransactionDetailsBean where fromAccount = :account_no");
		query2.setParameter("account_no",AccountDetailsBean.getAccountNumber());
		list=query2.list();
		System.out.println(list);
		if(list!=null && list.size()>0){
		for(TransactionDetailsBean t:list)
		{
			System.out.println(t);
		}
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}

	public List<TransactionDetailsBean> fundReceivedHistory(int userId) {

		List<TransactionDetailsBean> list=null;
		List<AccountDetailsBean> AccountDetailsBeans=null;
		AccountDetailsBean AccountDetailsBean=null;
		try
		{
	    System.out.println("Fetching record.....................");
		Session session=sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("from AccountDetailsBean where customerBean.customerId = :id");
		query.setParameter("id",userId);
		AccountDetailsBeans=query.list();
		
		for(AccountDetailsBean a:AccountDetailsBeans)
		{
			System.out.println(a);
		}
		
		if(AccountDetailsBeans!=null){
		AccountDetailsBean=AccountDetailsBeans.get(0);
		}
		
		System.out.println(AccountDetailsBean);
		
		Query query2 = session.createQuery("from TransactionDetailsBean where toAccount = :account_no");
		query2.setParameter("account_no",AccountDetailsBean.getAccountNumber());
		list=query2.list();
		
		for(TransactionDetailsBean t:list)
		{
			System.out.println(t);
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
	
	// Transaction History By Date
	
	public List<TransactionDetailsBean> fundHistoryByDate(int userId, String fromDate,
			String toDate) {

		List<TransactionDetailsBean> list=null;
		List<AccountDetailsBean> AccountDetailsBeans=null;
		AccountDetailsBean AccountDetailsBean=null;
		try
		{
	    System.out.println("Fetching record.....................");
		Session session=sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("from AccountDetailsBean where customerBean.customerId = :id");
		query.setParameter("id",userId);
		AccountDetailsBeans=query.list();
		
		if(AccountDetailsBeans!=null){
		for(AccountDetailsBean a:AccountDetailsBeans)
		{
			System.out.println(a);
		}
		AccountDetailsBean=AccountDetailsBeans.get(0);
		}
		
		System.out.println(AccountDetailsBean);
		
		Query query2 = session.createQuery("from TransactionDetailsBean where transactionDate >= :fromDate AND transactionDate <= :toDate");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date1=formatter.parse(fromDate); 
		Date date2=formatter.parse(toDate+1); 
		query2.setParameter("fromDate",date1);
		query2.setParameter("toDate",date2);
		list=query2.list();
		System.out.println(list);
		if(list!=null && list.size()>0){
		for(TransactionDetailsBean t:list)
		{
			System.out.println(t);
		}
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
	
	
	public TransactionDetailsBean TxHistoryByTransactionID(String txID) {
		TransactionDetailsBean bean=null;
		try
		{
			Session session=sessionFactory.getCurrentSession();
			bean=session.get(TransactionDetailsBean.class,txID);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		return bean;
	}

	public List<TransactionDetailsBean> fundHistoryByDateforUser(int userId,String fromDate, String toDate) {
		
		List<TransactionDetailsBean> list=null;
		List<AccountDetailsBean> AccountDetailsBeans=null;
		AccountDetailsBean AccountDetailsBean=null;
		int accountNumber=0;
		try
		{
	    System.out.println("Fetching record.....................");
		Session session=sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("from AccountDetailsBean where customerBean.customerId = :id");
		query.setParameter("id",userId);
		AccountDetailsBeans=query.list();
		
		if(AccountDetailsBeans!=null){
		for(AccountDetailsBean a:AccountDetailsBeans)
		{
			System.out.println(a);
		}
		AccountDetailsBean=AccountDetailsBeans.get(0);
		accountNumber=AccountDetailsBean.getAccountNumber();
		}
		
		
		Query query2 = session.createQuery("from TransactionDetailsBean where transactionDate >= :fromDate AND transactionDate <= :toDate AND (toAccount = :acc_to OR fromAccount = :acc_from)");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date1=formatter.parse(fromDate); 
		Date date2=formatter.parse(toDate+1); 
		query2.setParameter("fromDate",date1);
		query2.setParameter("toDate",date2);
		query2.setParameter("acc_to",accountNumber);
		query2.setParameter("acc_from",accountNumber);
		list=query2.list();
		System.out.println(list);
		if(list!=null && list.size()>0){
		for(TransactionDetailsBean t:list)
		{
			System.out.println(t);
		}
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}

	
	
	
}
