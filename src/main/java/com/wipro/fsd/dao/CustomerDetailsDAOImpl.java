package com.wipro.fsd.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wipro.fsd.bean.CustomerDetailsBean;
import com.wipro.fsd.bean.UserDetailsBean;

@Repository
public class CustomerDetailsDAOImpl implements CustomerDetailsDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<CustomerDetailsBean> viewAccountByCustomerId(
			String customerId) {

		List<CustomerDetailsBean> list = null;
		try {
			System.out.println("Fetching record.....................");
			Session session = sessionFactory.getCurrentSession();

			Query query = session.createQuery(
					"from CustomerDetailsBean c where c.customerId = :id");
			query.setParameter("id", Integer.parseInt(customerId));
			list = query.list();

			for (CustomerDetailsBean account : list) {
				System.out.println(account.getCustomerId());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<CustomerDetailsBean> viewAccountByAccountNumber(int id) {

		List<CustomerDetailsBean> customer = null;
		try {
			System.out.println("Fetching record.....................");
			Session session = sessionFactory.getCurrentSession();

			Criteria criteria = session
					.createCriteria(CustomerDetailsBean.class);
			criteria.add(Restrictions.eq("customerId", id));
			customer = criteria.list();

			// System.out.println("Customer: "+customer.get(0).getFirstName());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return customer;
	}

	@Override
	public String updateCustomerAddress(String customerId,
			String customerAddress) {

		String flag = null;
		try {
			System.out.println("Fetching record.....................");
			Session session = sessionFactory.getCurrentSession();
			// session.beginTransaction();
			CustomerDetailsBean customer = session.get(
					CustomerDetailsBean.class, Integer.parseInt(customerId));
			customer.setAddress(customerAddress);
			session.update(customer);
			// session.getTransaction().commit();
			flag = "success";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public String deleteCustomer(String customerId) {

		String flag = null;

		try {
			System.out.println("Fetching record.....................");
			Session session = sessionFactory.getCurrentSession();

			System.out.println("Deleting customer......");
			CustomerDetailsBean customer = session.get(
					CustomerDetailsBean.class, Integer.parseInt(customerId));
			session.delete(customer);

			System.out.println("Closing account........");
			String hql = "delete from AccountDetailsBean where customerBean.customerId= :cust_Id";
			session.createQuery(hql)
					.setParameter("cust_Id", Integer.parseInt(customerId))
					.executeUpdate();

			System.out.println("Disabling netbanking........");
			UserDetailsBean user = session.get(UserDetailsBean.class,
					Integer.parseInt(customerId));
			session.delete(user);

			flag = "success";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;

	}

	@Override
	public CustomerDetailsBean customerPersonalDetails(String cust_id) {

		CustomerDetailsBean customer = null;
		try {
			System.out.println("Fetching record.....................");
			Session session = sessionFactory.getCurrentSession();
			customer = session.get(CustomerDetailsBean.class,
					Integer.parseInt(cust_id));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return customer;
	}
}
