package com.wipro.fsd.dao;

import java.util.List;

import com.wipro.fsd.bean.AccountDetailsBean;
import com.wipro.fsd.bean.CompleteDetailsBean;

public interface AccountDetailsDAO {

	
	String saveAccount(CompleteDetailsBean complete_details);

	List<AccountDetailsBean> viewAccountDetailsByCustomerId(String customerId);

	List<AccountDetailsBean> viewAccountDetailsByAccountNumber(String accountNumber);
	
	
	
}
