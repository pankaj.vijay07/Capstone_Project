package com.wipro.fsd.dao;

import java.util.List;
import java.util.Random;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wipro.fsd.bean.AccountDetailsBean;
import com.wipro.fsd.bean.CompleteDetailsBean;
import com.wipro.fsd.bean.CustomerDetailsBean;
import com.wipro.fsd.bean.UserDetailsBean;

@Repository
public class AccountDetailsDAOImpl implements AccountDetailsDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public SessionFactory getSessionFactory() {
			return sessionFactory;
		}

		public void setSessionFactory(SessionFactory sessionFactory) {
			this.sessionFactory = sessionFactory;
		}

	@Override
	public String saveAccount(CompleteDetailsBean complete_details) {

		String flag="failure";
		try
		{
	    
		Session session=sessionFactory.getCurrentSession();
		complete_details.setCustomerId(getCustomerId());
		
		CustomerDetailsBean customer= createNewCustomer(complete_details);
		session.save(customer);
		System.out.println("CustomerID: "+customer.getCustomerId());
		
		AccountDetailsBean account=openAccount(complete_details);
		account.setCustomerDetailsBean(customer);
		session.save(account);
		System.out.println("Account Number: "+account.getAccountNumber());
		
		UserDetailsBean user=new UserDetailsBean();
		user=allowNetBankingForUser(complete_details);
		session.save(user);
		System.out.println("Netbanking Enabled.");
		flag=account.getAccountNumber()+"";

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return flag;
	}

	private CustomerDetailsBean createNewCustomer(CompleteDetailsBean complete_details) {
		
		CustomerDetailsBean customer=new CustomerDetailsBean();
		customer.setCustomerId(complete_details.getCustomerId());
		customer.setCustomerType(complete_details.getCustomerType());
		customer.setName(complete_details.getName());;
		customer.setFatherName(complete_details.getFatherName());
		customer.setMotherName(complete_details.getMotherName());
		customer.setDateOfBirth(complete_details.getDateOfBirth());
		customer.setGender(complete_details.getGender());
		customer.setContactNumber(complete_details.getContactNumber());
		customer.setEmail(complete_details.getEmail());
		customer.setAddress(complete_details.getCustomer_address());
		customer.setPancardNumber(complete_details.getPanCardNumber());
		
		return customer;
			
}
	private AccountDetailsBean openAccount(CompleteDetailsBean complete_details) {
		
		AccountDetailsBean account=new AccountDetailsBean();
		account.setAccountNumber(getAccountNumber());
		account.setAccountType(complete_details.getAccountType());
		account.setAccountBalance(complete_details.getAccountBalance());
		account.setBankName(complete_details.getBankName());
		account.setIfscCode(complete_details.getIfscCode());
		account.setMicrNumber(complete_details.getMicrNumber());
		account.setPancardNumber(complete_details.getPanCardNumber());
		account.setBankAddress(complete_details.getBank_address());
		
		return account;
			
}	
	// auto-generated customerId
			public int getCustomerId(){
			
			int id=0;
			Random r=new Random();
			id=Math.abs(r.nextInt(1000)*r.nextInt(1000)*r.nextInt(1000)+r.nextInt(100)*r.nextInt(100));
			
			return id;		
		}
	
			// auto-generated account number
						public int getAccountNumber(){
						
						int id=0;
						Random r=new Random();
						id=Math.abs(r.nextInt(10000)*r.nextInt(1000)*r.nextInt(1000)+
								r.nextInt(100)*r.nextInt(100)+r.nextInt(1000)*r.nextInt(1000)+1000000000);
						
						return id;		
					}
	
	
	
	
	@Override
	public List<AccountDetailsBean> viewAccountDetailsByCustomerId(
			String customerId) {
		
		List<AccountDetailsBean> list=null;
		try
		{
	    System.out.println("Fetching record.....................");
		Session session=sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("from AccountDetailsBean where customerDetailsBean.customerId = :id");
		query.setParameter("id", Integer.parseInt(customerId));
		list=query.list();
		
		for(AccountDetailsBean account:list){
			System.out.println(account.getAccountNumber());
		}
	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<AccountDetailsBean> viewAccountDetailsByAccountNumber(
			String accountNumber) {
		
		List<AccountDetailsBean> list=null;
		try
		{
	    
		Session session=sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("from AccountDetailsBean c where c.accountNumber = :id");
		query.setParameter("id", Integer.parseInt(accountNumber));
		list=query.list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
	
	
	public UserDetailsBean allowNetBankingForUser(CompleteDetailsBean complete_details){

		
		Random r=new Random();
		String id=r.nextInt(10000)*r.nextInt(10000)+"";
		String password=complete_details.getName().substring(0,3).toUpperCase()+id;
		
		UserDetailsBean user=new UserDetailsBean();
		user.setUserId(complete_details.getCustomerId());
		user.setUserName(complete_details.getName());
		user.setUserType("User");
		user.setPassword(password);
		
		System.out.println("Password is being generated for netbanking: "+password);
		return user;		
	}

}
