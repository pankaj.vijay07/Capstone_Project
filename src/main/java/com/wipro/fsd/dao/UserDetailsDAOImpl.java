package com.wipro.fsd.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wipro.fsd.bean.UserDetailsBean;

@Repository
public class UserDetailsDAOImpl implements UserDetailsDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public String save(UserDetailsBean emp) {
		// TODO Auto-generated method stub

		String flag = "failure";
		try {
			System.out.println("Saving the record.......");
			Session session = sessionFactory.getCurrentSession();
			int s = (Integer) session.save(emp);
			flag = "" + s;

			System.out.println("Employee Record saved.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;

	}

	@Override
	public UserDetailsBean login(UserDetailsBean user) {
		// TODO Auto-generated method stub
		UserDetailsBean userDB = null;
		try {
			System.out.println("Fetching record.....................");
			Session session = sessionFactory.getCurrentSession();

			userDB = (UserDetailsBean) session.get(UserDetailsBean.class,
					user.getUserId());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDB;
	}

	@Override
	public String changePassword(int userId, String newPassword) {
		// TODO Auto-generated method stub

		String flag = "failure";
		try {

			System.out.println("Changing password.......");
			Session session = sessionFactory.getCurrentSession();

			UserDetailsBean user = session.get(UserDetailsBean.class, userId);
			user.setPassword(newPassword);
			session.update(user);
			flag = "success";

			System.out.println("Employee Record saved.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

}
