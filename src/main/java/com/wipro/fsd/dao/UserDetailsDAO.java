package com.wipro.fsd.dao;

import com.wipro.fsd.bean.UserDetailsBean;

public interface UserDetailsDAO {

	public String save(UserDetailsBean emp);

	public UserDetailsBean login(UserDetailsBean user);

	public String changePassword(int userId,String newPassword);
	
	
	
	
}
