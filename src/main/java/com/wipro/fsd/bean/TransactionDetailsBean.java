package com.wipro.fsd.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
//This is a POJO class  which maps java class object to the database table object with the table name transaction_details

@Entity
@Table(name="transaction_details")
public class TransactionDetailsBean {

	@Id
	private String transactionId;
	private int fromAccount;
	private int toAccount;
	private String accountType;
	private String transferredAmount;
	private Date dateOfTransaction;
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public int getFromAccount() {
		return fromAccount;
	}
	public void setFromAccount(int fromAccount) {
		this.fromAccount = fromAccount;
	}
	public int getToAccount() {
		return toAccount;
	}
	public void setToAccount(int toAccount) {
		this.toAccount = toAccount;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getTransferredAmount() {
		return transferredAmount;
	}
	public void setTransferredAmount(String transferredAmount) {
		this.transferredAmount = transferredAmount;
	}

	public Date getDateOfTransaction() {
		return dateOfTransaction;
	}
	public void setDateOfTransaction(Date dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}
	@Override
	public String toString() {
		return "TransactionDetailsBean [transactionId=" + transactionId
				+ ", fromAccount=" + fromAccount + ", toAccount=" + toAccount
				+ ", accountType=" + accountType + ", transferredAmount="
				+ transferredAmount + ", dateOfTransaction=" + dateOfTransaction
				+ "]";
	}
}
