package com.wipro.fsd.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
//This is a POJO class  which maps java class object to the database table object with the table name user_details


@Entity
@Table(name="user_details")
public class UserDetailsBean {

	
	@Id
	private int userId;
	private String userName;
	private String password;
	private String userType;
	
	
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	@Override
	public String toString() {
		return "UserDetailsBean [userId=" + userId + ", password=" + password
				+ ", userType=" + userType + ", userName=" + userName + "]";
	}
}
