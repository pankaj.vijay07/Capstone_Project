package com.wipro.fsd.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
//This is a POJO class  which maps java class object to the database table object with the table name customer_details


@Entity
@Table(name = "customer_details")
public class CustomerDetailsBean {

	@Id
	private int customerId;
	private String customerType;
	private String name;
	private String fatherName;
	private String motherName;
	private String dateOfBirth;
	private String gender;
	private long contactNumber;
	private String email;
	private String address;
	private String pancardNumber;

	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPancardNumber() {
		return pancardNumber;
	}
	public void setPancardNumber(String pancardNumber) {
		this.pancardNumber = pancardNumber;
	}
	@Override
	public String toString() {
		return "CustomerDetailsBean [customerId=" + customerId
				+ ", customerType=" + customerType + ", name=" + name
				+ ", fatherName=" + fatherName + ", motherName=" + motherName
				+ ", dateOfBirth=" + dateOfBirth + ", gender=" + gender
				+ ", contactNumber=" + contactNumber + ", email=" + email
				+ ", address=" + address + ", pancardNumber=" + pancardNumber
				+ "]";
	}
}
