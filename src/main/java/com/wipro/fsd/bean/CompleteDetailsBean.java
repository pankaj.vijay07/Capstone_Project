package com.wipro.fsd.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
//This is a POJO class  which maps java object to the database table object with the table name complete_details


@Entity
@Table(name="complete_details")
public class CompleteDetailsBean {

	@Id
	@GeneratedValue
	private int accountNumber;
	private int customerId;
	private String customerType;
	private String name;
	private String fatherName;
	private String motherName;
	private String dateOfBirth;
	private String gender;
	private long contactNumber;
	private String accountType;
	private String accountBalance;
	private String panCardNumber;
	private String bankName;
	private String ifscCode;
	private long micrNumber;
	private String customer_address;
	private String bank_address;
	
	public String getCustomer_address() {
		return customer_address;
	}
	public void setCustomer_address(String customer_address) {
		this.customer_address = customer_address;
	}
	public String getBank_address() {
		return bank_address;
	}
	public void setBank_address(String bank_address) {
		this.bank_address = bank_address;
	}
	public int getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(String accountBalance) {
		this.accountBalance = accountBalance;
	}
	public String getPanCardNumber() {
		return panCardNumber;
	}
	public void setPanCardNumber(String panCardNumber) {
		this.panCardNumber = panCardNumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	public long getMicrNumber() {
		return micrNumber;
	}
	public void setMicrNumber(long micrNumber) {
		this.micrNumber = micrNumber;
	}
	@Override
	public String toString() {
		return "CompleteDetailsBean [accountNumber=" + accountNumber
				+ ", customerId=" + customerId + ", customerType="
				+ customerType + ", name=" + name + ", fatherName=" + fatherName
				+ ", motherName=" + motherName + ", dateOfBirth=" + dateOfBirth
				+ ", gender=" + gender + ", contactNumber=" + contactNumber
				+ ", accountType=" + accountType
				+ ", accountBalance=" + accountBalance + ", panCardNumber="
				+ panCardNumber + ", bankName=" + bankName + ", ifscCode="
				+ ifscCode + ", micrNumber=" + micrNumber
				+ ", customer_address=" + customer_address + ", bank_address="
				+ bank_address + "]";
	}

}
