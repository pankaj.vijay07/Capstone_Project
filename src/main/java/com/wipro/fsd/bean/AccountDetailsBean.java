package com.wipro.fsd.bean;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//This is a POJO class  which maps java object to the database table object with the name as account_details



@Entity
@Table(name = "account_details")
public class AccountDetailsBean {

	@Id
	private int accountNumber;
	private String accountType;
	private String accountBalance;
	private String pancardNumber;
	private String bankName;
	private String ifscCode;
	private long micrNumber;
	private String bankAddress;

	@ManyToOne(cascade = CascadeType.ALL)
	CustomerDetailsBean customerDetailsBean;

	public int getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(String accountBalance) {
		this.accountBalance = accountBalance;
	}
	public String getPancardNumber() {
		return pancardNumber;
	}
	public void setPancardNumber(String pancardNumber) {
		this.pancardNumber = pancardNumber;
	}
	public CustomerDetailsBean getCustomerDetailsBean() {
		return customerDetailsBean;
	}
	public void setCustomerDetailsBean(
			CustomerDetailsBean customerDetailsBean) {
		this.customerDetailsBean = customerDetailsBean;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public long getMicrNumber() {
		return micrNumber;
	}
	public void setMicrNumber(long micrNumber) {
		this.micrNumber = micrNumber;
	}
	public String getBankAddress() {
		return bankAddress;
	}
	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}
	@Override
	public String toString() {
		return "AccountDetailsBean [accountNumber=" + accountNumber
				+ ", accountType=" + accountType + ", accountBalance="
				+ accountBalance + ", pancardNumber=" + pancardNumber
				+ ", bankName=" + bankName + ", ifscCode=" + ifscCode
				+ ", micrNumber=" + micrNumber + ", bankAddress=" + bankAddress
				+ ", customerDetailsBean=" + customerDetailsBean + "]";
	}

}
