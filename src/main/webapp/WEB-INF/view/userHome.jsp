<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Home Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
}
</script>

</head>
<body onload="noBack();" bgcolor="green">
<nav>
<a href="/rest/fundTransferPage">Fund Transfer</a>|
<a href="/rest/AccountInfoPage">Account</a>|
<a href="/rest/personalDetailsPage">Personal Details</a>|
<a href="/rest/updateCustomerDetailsPage">Update Details</a>| 
<a href="/rest/transactionHistoryPage">Transaction History</a>| 
<a href="/rest/changeUserPasswordPage">Change Password</a>|
<a href="/rest/logOutPage">Log Out</a>
</nav>
<c:if test="${!empty accountBeans}">
<table border="1">
<tr>
<th>Account Number</th>
<!-- <th>Account Type</th>
<th>Bank Name</th>
<th>IFSC Code</th>
<th>Branch Name</th> -->
<th>Balance</th>
</c:if>
</tr>
	<c:forEach var="account" items="${accountBeans}" varStatus="count">
<tr> 
		<td><c:out value="${account.accountNumber}"></c:out></td>
<%-- 		<td><c:out value="${account.accoutType}"></c:out></td>
		<td><c:out value="${account.bankName}"></c:out></td>
		<td><c:out value="${account.ifscCode}"></c:out></td>
		<td><c:out value="${account.branchName}"></c:out></td> --%>
		<td><c:out value="${account.balance}"></c:out></td>

</tr>
</c:forEach>
</table>
<br>
<h1 style="color:#f2f2f2">${msg}</h1>
</body>
</html>