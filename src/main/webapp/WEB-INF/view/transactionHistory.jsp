<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Transaction Details Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
	
	myFun();
}
</script>

<script type="text/javascript">

function fundTransferedHistory() {
	
	var x= document.getElementById("fundTransferedHistory");
	
	document.getElementById("myForm").style.display ="block";
		x.style.display = "block";
	
	document.getElementById("fundReceivedHistory").style.display="none";
	document.getElementById("dateTodate").style.display="none"; 
}

function fundReceivedHistory() {
	
	var x= document.getElementById("fundReceivedHistory");
	
	document.getElementById("myForm").style.display ="block";
		x.style.display = "block";
	
	document.getElementById("fundTransferedHistory").style.display="none";
	document.getElementById("dateTodate").style.display="none"; 
}

function dateTodate() {
	
	var x= document.getElementById("dateTodate");
	
	document.getElementById("myForm").style.display ="block";
		x.style.display = "block";
	
	document.getElementById("fundTransferedHistory").style.display="none";
	document.getElementById("fundReceivedHistory").style.display="none";
}

function myFun() {
	
	document.getElementById("myForm").style.display ="none";	
	document.getElementById("fundTransferedHistory").style.display ="none";	
	document.getElementById("dateTodate").style.display="none";
	document.getElementById("fundReceivedHistory").style.display="none";
}
</script> 

</head>
<body onload="noBack();" bgcolor="green">
<header>
<p align="right">
<a href="/rest/fundTransferPage"><font color="yellow">Fund Transfer</font></a>
<a href="/rest/AccountInfoPage"><font color="yellow">Account</font></a>
<a href="/rest/personalDetailsPage"><font color="yellow">Personal Details</font></a>
<a href="/rest/updateCustomerDetailsPage"><font color="yellow">Update Details</font></a> 
<a href="/rest/transactionHistoryPage"><font color="yellow">Transaction History</font></a> 
<a href="/rest/changeUserPasswordPage"><font color="yellow">Change Password</font></a>
<a href="/rest/logOutPage"><font color="yellow">Log Out</font></a>
</p>
</header>
<div style="color:red;background-color:yellow"><h1>Show Details</h1></div>

<form>
<input type="radio" name="frstName" value="fundTransferedHistory" onclick="fundTransferedHistory()"></input> Fund Transfered History<br><br>
<input type="radio" name="frstName" value="fundReceivedHistory" onclick="fundReceivedHistory()"></input> Fund Received History<br><br>
<input type="radio" name="frstName" value="findByAccountNumber" onclick="dateTodate()"> History By Date</input><br><br>
</form>

<div id="myForm">
<div id="fundTransferedHistory" display="none">
<form action="/rest/fundTransferedHistory" method="post">
<input type="submit" value="Show" style="background-color:#11C656"/>
</form>
</div>

<div id="fundReceivedHistory" display="none">
<form action="/rest/fundReceivedHistory" method="post">
<input type="submit" value="Show" style="background-color:#11C656"/>
</form>
</div>

<div id="dateTodate" display="none">
<form action="/rest/fundHistoryByDate" method="post">
<input type="date" name="fromDate" placeholder="Enter From Date" required/><br>
<input type="date" name="toDate" placeholder="Enter To Date" required/><br>
<input type="submit" value="show" style="background-color:#11C656"/>
</form>
</div>
</div>

<c:if test="${!empty list}">
<table border="1">
<tr>
<th>TransactionId</th>
<th>From Account</th>
<th>To Account</th>
<th>AccoutType</th>
<th>Bank Name</th>
<th>Amount</th>
<th>Transaction Time</th>
</c:if>
</tr>

<c:forEach var="bean" items="${list}" varStatus="count1">
<tr>
	<td><c:out value="${bean.transactionId}"></c:out></td>
	<td><c:out value="${bean.fromAccount}"></c:out></td>
	<td><c:out value="${bean.toAccount}"></c:out></td>
	<td><c:out value="${bean.accountType}"></c:out></td>
	<td><c:out value="${bean.transferredAmount}"></c:out></td>
	<td><c:out value="${bean.dateOfTransaction}"></c:out></td>
</tr>
</c:forEach>
</table>
<c:if test="${empty list}">
		<h2>No record found! Please try again later!</h2>
	</c:if>
</body>
</html>