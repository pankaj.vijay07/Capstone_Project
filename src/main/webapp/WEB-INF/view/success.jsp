<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="/css/image.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Success Page</title>
</head>
<body bgcolor="green">
<h1 style="color:#f2f2f2">
${msg}<br>
${msg2}
</h1>
<br>
<div style="color:#f2f2f2">
<a href="/rest/home">Home</a>
</div>
</body>
</html>