<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delete Customer Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
}
</script>

</head>
<body onload="noBack();" bgcolor="green">
<header>
<p align="right">
<a href="/rest/openAccountPage"><font color="yellow">Open Account</font></a>
<a href="/rest/showCustomerPage"><font color="yellow">Show Customer</font></a>
<a href="/rest/updateCustomerPage"><font color="yellow">Update Customer</font></a>
<a href="/rest/deleteCustomerPage"><font color="yellow">Delete Customer</font></a>
<a href="/rest/changePasswordPage"><font color="yellow">Change Password</font></a>
<a href="/rest/logOutPage"><font color="yellow">Log Out</font></a>
</p>
</header>
<div style="color:red;backgroung-color:yellow"><h1>Delete Customer</h1></div>
<center>
<form action="/rest/deleteCustomer" method="post">
<table>
<tr>
<td>Enter CustomerId</td>
<td><input type="number" name="customerId" placeholder="enter customerId" required="required"/></td>
</tr>
<tr>
<td></td>
<td><input type="submit" value="Delete"/></td>
</tr>
</table>
</form>
</center>
</body>
</html>