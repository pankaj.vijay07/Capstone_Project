<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Customer Details Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
	
	myFun();
}
</script>

<script type="text/javascript">

function updateCustomerPhone() {
	
	var x= document.getElementById("updateCustomerPhone");
	
	document.getElementById("myForm").style.display ="block";
		x.style.display = "block";
	
	document.getElementById("updateCustomerAddress").style.display="none"; 
}

function updateCustomerAddress() {
	
	var x= document.getElementById("updateCustomerAddress");
	
	document.getElementById("myForm").style.display ="block";
		x.style.display = "block";
	
	document.getElementById("updateCustomerPhone").style.display="none";
}

function myFun() {
	
	document.getElementById("myForm").style.display ="none";	
	document.getElementById("updateCustomerAddress").style.display="none";
	document.getElementById("updateCustomerPhone").style.display="none";
}
</script> 

</head>
<body onload="noBack();" bgcolor="green">
<header>
<p align="right">
<a href="/rest/openAccountPage"><font color="yellow">Open Account</font></a>
<a href="/rest/showCustomerPage"><font color="yellow">Show Customer</font></a>
<a href="/rest/updateCustomerPage"><font color="yellow">Update Customer</font></a> 
<a href="/rest/deleteCustomerPage"><font color="yellow">Delete Customer</font></a>
<a href="/rest/sendMailToCustomerPage"><font color="yellow">Send Mail</font></a>
<a href="/rest/changePasswordPage"><font color="yellow">Change Password</font></a>
<a href="/rest/logOutPage"><font color="yellow">Log Out</font></a>
</p>
</header>
<div style="color:red;background-color:yellow;"><h1>Update Details</h1></div>

<form>
<input type="radio" name="frstName" value="updateCustomerPhone" onclick="updateCustomerPhone()"></input> Update Phone<br><br>
<input type="radio" name="frstName" value="updateCustomerAddress" onclick="updateCustomerAddress()"></input>Update Address<br><br>
</form>

<div id="myForm">
<div id="updateCustomerPhone" display="none">
<form action="/rest/updateCustomerPhone" method="post">
<input type="text" name="updateCustomerId" placeholder="Enter CustomerId" required/><br>
<input type="text" name="updateCustomerPhone" placeholder="Enter New Phone" required/><br>
<input type="submit" value="Update Phone" style="background-color:#11C656"/>
</form>
</div>

<div id="updateCustomerAddress" display="none">
<form action="/rest/updateCustomerAddress" method="post">
<input type="text" name="updateCustomerId" placeholder="Enter CustomerId" required/><br>
<input type="text" name="updateCustomerAddress" placeholder="Enter New Address" required/><br>
<input type="submit" value="Update Address" style="background-color:#11C656"/>
</form>
</div>
</div>

<c:if test="${!empty customerlist}">
<table border="1">
<tr>
<th>Customer Id</th>
<th>Name</th>
<th>Father's Name</th>
<th>Mother's Name</th>
<th>DOB</th>
<th>Gender</th>
<th>Phone</th>
<th>Address</th>
<th>PAN Number</th>
<c:if test="${!empty accountlist}">
<th>Account Number</th>
<th>Account Type</th>
<th>Bank Name</th>
<th>IFSC Code</th>
<th>MICR Number</th>
<th>Account Balance</th>
</c:if>
</tr>

<c:forEach var="bean" items="${customerlist}" varStatus="count1">
	<c:forEach var="account" items="${accountlist}" varStatus="count2">
<tr>
	<td><c:out value="${bean.customerId}"></c:out></td>
	<td><c:out value="${bean.name}"></c:out></td>
	<td><c:out value="${bean.fatherName}"></c:out></td>
	<td><c:out value="${bean.motherName}"></c:out></td>
	<td><c:out value="${bean.dateOfBirth}"></c:out></td>
	<td><c:out value="${bean.gender}"></c:out></td>
	<td><c:out value="${bean.contactNumber}"></c:out></td>
	<td><c:out value="${bean.customer_address}"></c:out></td>
	<td><c:out value="${bean.pancardNumber}"></c:out></td> 

		<td><c:out value="${account.accountNumber}"></c:out></td>
		<td><c:out value="${account.accountType}"></c:out></td>
		<td><c:out value="${account.bankName}"></c:out></td>
		<td><c:out value="${account.ifscCode}"></c:out></td>
		<td><c:out value="${account.micrNumber}"></c:out></td>
		<td><c:out value="${account.accountBalance}"></c:out></td>

</tr>
</c:forEach>
</c:forEach>
</table>
</c:if>

</body>
</html>