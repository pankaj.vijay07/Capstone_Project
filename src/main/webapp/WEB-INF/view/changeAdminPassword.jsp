<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Change Password Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
}
</script>

</head>
<body onload="noBack();" bgcolor="green">
<nav>
<p align="right">
<a href="/rest/openAccountPage"><font color="yellow">Open Account</font></a>
<a href="/rest/showCustomerPage"><font color="yellow">Show Customer</font></a>
<a href="/rest/updateCustomerPage"><font color="yellow">Update Customer</font></a> 
<a href="/rest/deleteCustomerPage"><font color="yellow">Delete Customer</font></a>
<a href="/rest/changePasswordPage"><font color="yellow">Change Password</font></a>
<a href="/rest/logOutPage"><font color="yellow">Log Out</font></a>
</p>
</nav>
<div style="color:#f2f2f2;padding:20px;"><h1>Change Password</h1></div>
<center>
<form action="/rest/changeMyPassword" method="post">
<input type="password" name="newPassword" placeholder="enter new Password" required="required"/>
<input type="submit" value="Change Password" style="background-color:#11C656"/>
</form>
</center>

</body>
</html>