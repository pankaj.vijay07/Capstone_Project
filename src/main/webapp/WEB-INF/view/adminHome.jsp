<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome Admin Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
}
</script>

</head>
<body onload="noBack();" bgcolor="green">
<header>
<a href="/rest/openAccountPage">Open Account</a>|
<a href="/rest/showCustomerPage">Show Customer</a>|
<a href="/rest/updateCustomerPage">Update Customer</a> |
<a href="/rest/deleteCustomerPage">Delete Customer</a>|
<a href="/rest/transactionHistoryAdminPage">Transaction History</a> |
<a href="/rest/changePasswordPage">Change Password</a>|
<a href="/rest/logOutPage">Log Out</a>
</header>
<br>
<h1 style="color:#f2f2f2">${msg}</h1>
</body>
</html>