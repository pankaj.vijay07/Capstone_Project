<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Info Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
	
	myFun();
}
</script>

<script type="text/javascript">

function findByCustomerId() {
	
	var x= document.getElementById("findByCustomerId");
	
	document.getElementById("myForm").style.display ="block";
		x.style.display = "block";
	
	document.getElementById("findByAccountNumber").style.display="none"; 
}

function findByAccountNumber() {
	
	var x= document.getElementById("findByAccountNumber");
	
	document.getElementById("myForm").style.display ="block";
		x.style.display = "block";
	
	document.getElementById("findByCustomerId").style.display="none";
}

function myFun() {
	
	document.getElementById("myForm").style.display ="none";	
	document.getElementById("findByCustomerId").style.display ="none";	
	document.getElementById("findByAccountNumber").style.display="none";
	document.getElementById("findByPAN").style.display="none";
}
</script> 

</head>
<body onload="noBack();" bgcolor="green">
<header>

<a href="/rest/openAccountPage"><font color="yellow">Open Account</font></a>
<a href="/rest/showCustomerPage"><font color="yellow">Show Customer</font></a>
<a href="/rest/updateCustomerPage"><font color="yellow">Update Customer</font></a> 
<a href="/rest/deleteCustomerPage"><font color="yellow">Delete Customer</font></a>
<a href="/rest/changePasswordPage"><font color="yellow">Change Password</font></a>
<a href="/rest/logOutPage"><font color="yellow">Log Out</font></a>

</header>
<div style="color:red;"><h1>Show Details</h1>

<form>
<input type="radio" name="firstName" value="findByCustomerId" onclick="findByCustomerId()"></input> Show By CustomerId<br><br>
<input type="radio" name="firstName" value="findByAccountNumber" onclick="findByAccountNumber()"> Show By Account Number</input><br><br>
</form>
</div>
<div id="myForm">
<div id="findByCustomerId" display="none">
<form action="/rest/showDetailsByCustomerId" method="post">
<input type="number" name="showDetailsByCustomerId" placeholder="Enter CustomerId" required/><br>
<input type="submit" value="Find" />
</form>
</div>

<div id="findByAccountNumber" display="none">
<form action="/rest/showDetailsByAccountNumber" method="post">
<input type="number" name="showDetailsByAccountNumber" placeholder="Enter Account Number" required/><br>
<input type="submit" value="Find"/>
</form>
</div>
</div>

<c:if test="${!empty customerlist}">
<table border="1">
<tr>
<th>Customer Id</th>
<th>Name /th>
<th>Father's Name</th>
<th>Mother's Name</th>
<th>DOB</th>
<th>Gender</th>
<th>Contact No.</th>
<th>Address</th>
<th>PAN Number</th>
<c:if test="${!empty accountlist}">
<th>Account Number</th>
<th>Account Type</th>
<th>Bank Name</th>
<th>IFSC Code</th>
<th>MICR Number</th>
<th>Account Balance</th>
</c:if>
</tr>

<c:forEach var="bean" items="${customerlist}" varStatus="count1">
	<c:forEach var="account" items="${accountlist}" varStatus="count2">
<tr>
	<td><c:out value="${bean.customerId}"></c:out></td>
	<td><c:out value="${bean.name}"></c:out></td>
	<td><c:out value="${bean.fatherName}"></c:out></td>
	<td><c:out value="${bean.motherName}"></c:out></td>
	<td><c:out value="${bean.dateOfBirth}"></c:out></td>
	<td><c:out value="${bean.gender}"></c:out></td>
	<td><c:out value="${bean.contactNumber}"></c:out></td>
	<td><c:out value="${bean.customer_address}"></c:out></td>
	<td><c:out value="${bean.pancardNumber}"></c:out></td> 

		<td><c:out value="${account.accountNumber}"></c:out></td>
		<td><c:out value="${account.accountType}"></c:out></td>
		<td><c:out value="${account.bankName}"></c:out></td>
		<td><c:out value="${account.ifscCode}"></c:out></td>
		<td><c:out value="${account.micrNumber}"></c:out></td>
		<td><c:out value="${account.accountBalance}"></c:out></td>

</tr>
</c:forEach>
</c:forEach>
</table>
</c:if>
<c:if test="${empty customerlist}">
		<h2>No record found! Please try again later!</h2>
	</c:if>
</body>
</html>