<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Open AccountPage</title>

<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
	}
</script>

</head>
<body onload="noBack();" bgcolor="green">
	<header>
	<p align="right">
		<a href="/rest/openAccountPage"><font color="yellow">Open
				Account</font></a> <a href="/rest/showCustomerPage"><font color="yellow">Show
				Customer</font></a> <a href="/rest/updateCustomerPage"><font color="yellow">Update
				Customer</font></a> <a href="/rest/deleteCustomerPage"><font color="yellow">Delete
				Customer</font></a> <a href="/rest/changePasswordPage"><font color="yellow">Change
				Password</font></a> <a href="/rest/logOutPage"><font color="yellow">Log
				Out</font></a>
	</p>
	</header>
	<div style="color: #f2f2f2; padding: 20px;">
		<h1>Open Account</h1>
	</div>
	<center>

		<form action="/rest/openAccount" method="post">
			<table>

				<tr>
					<td>Full Name:</td>
					<td><input type="text" name="name"
						placeholder="enter full Name" required="required" /></td>
				</tr>

				<tr>
					<td>Father's Name:</td>
					<td><input type="text" name="fatherName"
						placeholder="enter father's Name" required="required" /></td>
				</tr>


				<tr>
					<td>Mother's Name:</td>
					<td><input type="text" name="motherName"
						placeholder="enter mother's Name" required="required" /></td>
				</tr>
				<tr>
					<td>Date of Birth</td>
					<td><input type="date" name="dateOfBirth"
						placeholder="enter your D.O.B" required="required" /></td>
				</tr>


				<tr>
					<td>Gender</td>
					<td><select name="gender" placeholder="your gender" required>
							<option value="Male">Male
							<option />
							<option value="Female">Female
							<option />
					</select></td>
				</tr>
				<tr>
					<td>Contact Number</td>
					<td><input type="number" name="contactNumber"
						placeholder="enter your contact no." required /></td>
				</tr>
				<tr>
					<td>Address</td>
					<td><input type="text" name="customer_address"
						placeholder="enter your Address" required /></td>
				</tr>
				<tr>
					<td>Account Type</td>
					<td><select name="accountType" placeholder="Account Type"
						required>
							<option value="Saving">Saving
							<option />
							<option value="Current">Current
							<option />
					</select></td>
				</tr>
				<tr>
					<td>Bank Name</td>
					<td><select name="bankName" placeholder="Bank Name" required>
							<option value="Allahabad Bank">Allahabad Bank
							<option />
							<option value="HDFC">HDFC Bank
							<option />
							<option value="Axis">Axis Bank
							<option />
							<option value="ICICI">ICICI Bank
							<option />
							<option value="DIGI Bank">DIGI Bank
							<option />
							<option value="Dena Bank">Dena Bank
							<option />
							<option value="Vijaya Bank">Vijaya Bank
							<option />
							<option value="Punjab National Bank">Punjab National
								Bank
							<option />
							<option value="SBI">SBI
							<option />
							<option value="Corporation Bank">Corporation Bank
							<option />
					</select></td>
				</tr>
				<tr>
					<td>IFSC Code</td>
					<td><input type="text" name="ifscCode"
						placeholder="enter IFSC Code" required /></td>
				</tr>
				<tr>
					<td>MICR Number</td>
					<td><input type="text" name="micrNumber"
						placeholder="enter MICR Number" required /></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="number" name="accountBalance"
						placeholder="Balance" required /></td>
				</tr>
				<tr>
					<td>PanCard Number</td>
					<td><input type="text" name="pancardNumber"
						placeholder="enter pan Number" required /></td>
				</tr>
				<tr>
					<td>Bank Address</td>
					<td><input type="text" name="bank_address"
						placeholder="enter your bank Address" required /></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" value="Open Account" /></td>
				</tr>
			</table>
		</form>
	</center>
</body>
</html>