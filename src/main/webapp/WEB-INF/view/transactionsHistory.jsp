<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Past Transaction Details Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
	
	myFun();
}
</script>

<script type="text/javascript">

function transactionsHistory() {
	
	var x= document.getElementById("transactionsHistory");
	
	document.getElementById("myForm").style.display ="block";
		x.style.display = "block";

	document.getElementById("dateTodate").style.display="none"; 
}

function dateTodate() {
	
	var x= document.getElementById("dateTodate");
	
	document.getElementById("myForm").style.display ="block";
		x.style.display = "block";
	
	document.getElementById("transactionsHistory").style.display="none";
}

function myFun() {
	
	document.getElementById("myForm").style.display ="none";	
	document.getElementById("transactionsHistory").style.display ="none";	
	document.getElementById("dateTodate").style.display="none";
}
</script> 

</head>
<body onload="noBack();" bgcolor="green">
<header>
<p align="right">
<a href="/rest/openAccountPage"><font color="yellow">Open Account</font></a>
<a href="/rest/showCustomerPage"><font color="yellow">Show Customer</font></a>
<a href="/rest/updateCustomerPage"><font color="yellow">Update Customer</font></a> 
<a href="/rest/deleteCustomerPage"><font color="yellow">Delete Customer</font></a>
<a href="/rest/transactionHistoryAdminPage"><font color="yellow">Transaction History</font></a> 
<a href="/rest/changePasswordPage"><font color="yellow">Change Password</font></a>
<a href="/rest/logOutPage"><font color="yellow">Log Out</font></a>
</p>
</header>
<div style="color:red;background-color:yellow"><h1>Show Details</h1></div>

<form>
<input type="radio" name="frstName" value="transactionsHistory" onclick="transactionsHistory()"></input>Transaction By TxID<br><br>
<input type="radio" name="frstName" value="dateTodate" onclick="dateTodate()">Transactions By Date</input><br><br>
</form>

<div id="myForm">
<div id="transactionsHistory" display="none">
<form action="/rest/transactionsHistoryByTxID" method="post">
<input type="text" name="txID" placeholder="Enter Transaction ID" required/><br>
<input type="submit" value="Show"/>
</form>
</div>

<div id="dateTodate" display="none">
<form action="/rest/transactionsHistoryByDate2" method="post">
<input type="number" name="customerId" placeholder="Enter Customer Id" required/><br>
<input type="date" name="fromDate" placeholder="Enter From Date" required/><br>
<input type="date" name="toDate" placeholder="Enter To Date" required/><br>
<input type="submit" value="show"/>
</form>
</div>
</div>

<c:if test="${!empty list}">
<table border="1">
<tr>
<th>TransactionId</th>
<th>From Account</th>
<th>To Account</th>
<th>AccoutType</th>
<th>Bank Name</th>
<th>Amount</th>
<th>Transaction Time</th>
</c:if>
</tr>

<c:forEach var="bean" items="${list}" varStatus="count1">
<tr>
	<td><c:out value="${bean.transactionId}"></c:out></td>
	<td><c:out value="${bean.fromAccount}"></c:out></td>
	<td><c:out value="${bean.toAccount}"></c:out></td>
	<td><c:out value="${bean.accoutType}"></c:out></td>
	<td><c:out value="${bean.bankName}"></c:out></td>
	<td><c:out value="${bean.amount}"></c:out></td>
	<td><c:out value="${bean.transactionDate}"></c:out></td>
</tr>
</c:forEach>
</table>
<c:if test="${empty list}">
		<h2>No record exists! Please try again!</h2>
	</c:if>
</body>
</html>