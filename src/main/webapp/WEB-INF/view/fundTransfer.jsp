<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Fund Transfer Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
}
</script>
</head>
<body onload="noBack();" bgcolor="green">
<header>
<p align="right">
<a href="/rest/fundTransferPage"><font color="yellow">Fund Transfer</font></a>|
<a href="/rest/AccountInfoPage"><font color="yellow">Account</font></a>|
<a href="/rest/personalDetailsPage"><font color="yellow">Personal Details</font></a>|
<a href="/rest/updateCustomerDetailsPage"><font color="yellow">Update Details</font></a> |
<a href="/rest/transactionHistoryPage"><font color="yellow">Transaction History</font></a>| 
<a href="/rest/changeUserPasswordPage"><font color="yellow">Change Password</font></a>|
<a href="/rest/logOutPage"><font color="yellow">Log Out</font></a>
</p>
</header>
<div style="color:red;background-color:yellow;"><h1>Fund Transfer</h1></div>
<center>

<form action="/rest/fundTransfer" method="post">

<input type="number" name="beneficieryAccountNumber" placeholder="Beneficiery Account Number" required="required"/>
<input type="text" name="beneficieryName" placeholder="Beneficiery Name" required="required"/>

<select name="bankName" placeholder="Bank Name" required="required"> 
<option value="Allahabad Bank">Allahabad Bank<option/>
<option value="HDFC">HDFC Bank<option/>
<option value="Axis">Axis Bank<option/>
<option value="ICICI">ICICI Bank<option/>
<option value="DIGI Bank">DIGI Bank<option/>
<option value="Dena Bank">Dena Bank<option/>
<option value="Vijaya Bank">Vijaya Bank<option/>
<option value="Punjab National Bank">Punjab National Bank<option/>
<option value="SBI">SBI<option/>
<option value="Corporation Bank">Corporation Bank<option/>
</select>

<select name="accoutType" placeholder="Account Type" required="required">
<option value="Saving">Saving<option/>
<option value="Current">Current<option/>
</select>

<input type="text" name="amount" placeholder="Amount"  required="required"/>
<input type="number" name="phone" placeholder="Phone" required="required"/>
<input type="submit" value="Transfer"/>
</form>
</center>
</body>
</html>