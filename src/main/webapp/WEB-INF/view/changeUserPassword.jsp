<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delete Customer Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
}
</script>

</head>
<body onload="noBack();" bgcolor="green">
<header>
<li><a href="/rest/fundTransferPage">Fund Transfer</a></li>
<li><a href="/rest/AccountInfoPage">Account</a></li>
<li><a href="/rest/personalDetailsPage">Personal Details</a></li>
<li><a href="/rest/updateCustomerDetailsPage">Update Details</a></li> 
<li><a href="/rest/transactionHistoryPage">Transaction History</a></li> 
<li><a href="/rest/changeUserPasswordPage">Change Password</a></li>
<li><a href="/rest/logOutPage">Log Out</a>
</ul>
</div>
</header>
<div style="color:red;background-color:yellow"><h1>Change Password</h1></div>
<center>
<form action="/rest/changeUserPassword" method="post">
<table>
<tr>
<td>New Password</td>
<td><input type="password" name="newPassword" placeholder="enter new Password" required="required"/>
</td>
</tr>
<tr>
<td></td>
<td><input type="submit" value="Change Password"/>
</td>
</tr>
</table>
</form>
</center>
</body>
</html>