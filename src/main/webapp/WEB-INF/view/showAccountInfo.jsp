<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Info Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
}
</script>

</head>
<body onload="noBack();" bgcolor="green">
<header>
<p align="right">
<a href="/rest/fundTransferPage"><font color="yellow">Fund Transfer</font></a>
<a href="/rest/AccountInfoPage"><font color="yellow">Account</font></a>
<a href="/rest/personalDetailsPage"><font color="yellow">Personal Details</font></a>
<a href="/rest/updateCustomerDetailsPage"><font color="yellow">Update Details</font></a> 
<a href="/rest/transactionHistoryPage"><font color="yellow">Transaction History</font></a> 
<a href="/rest/changeUserPasswordPage"><font color="yellow">Change Password</font></a>
<a href="/rest/logOutPage"><font color="yellow">Log Out</font></a>
</p>

</header>
<c:if test="${!empty accountBeans}">
<table border="1">
<tr>
<th>Account Number</th>
<th>Account Type</th>
<th>Bank Name</th>
<th>IFSC Code</th>
<th>MICR Number</th>
<th>Account Balance</th>
</c:if>
</tr>
	<c:forEach var="account" items="${accountBeans}" varStatus="count">
<tr> 
		<td><c:out value="${account.accountNumber}"></c:out></td>
		<td><c:out value="${account.accountType}"></c:out></td>
		<td><c:out value="${account.bankName}"></c:out></td>
		<td><c:out value="${account.ifscCode}"></c:out></td>
		<td><c:out value="${account.micrNumber}"></c:out></td>
		<td><c:out value="${account.accountBalance}"></c:out></td>

</tr>
</c:forEach>
</table>
<br>
<%-- <h1 style="color:#f2f2f2">${msg}</h1> --%>
</body>
</html>