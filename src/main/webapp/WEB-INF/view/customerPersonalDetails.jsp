<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Personal Details Page</title>

<script type="text/javascript">
window.history.forward();
function noBack() {
	window.history.forward();
	
	myFun();
}
</script>


</head>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
<header>
<div class="nav">
<ul>
<li><a href="/rest/fundTransferPage">Fund Transfer</a></li>
<li><a href="/rest/AccountInfoPage">Account</a></li>
<li><a href="/rest/personalDetailsPage">Personal Details</a></li>
<li><a href="/rest/updateCustomerDetailsPage">Update Details</a></li> 
<li><a href="/rest/transactionHistoryPage">Transaction History</a></li> 
<li><a href="/rest/changeUserPasswordPage">Change Password</a></li>
<li><a href="/rest/logOutPage">Log Out</a>
</ul>
</div>
</header>
<div style="color:#f2f2f2;padding:20px;"><h1>Customer Personal Details</h1>


<c:if test="${!empty customerlist}">
<table border="1">
<tr>
<th>Customer Id</th>
<th>First Name</th>
<th>Last Number</th>
<th>DOB</th>
<th>Gender</th>
<th>Phone</th>
<th>Email</th>
<th>Address</th>
<th>PAN Number</th>
</tr>

<c:forEach var="bean" items="${customerlist}" varStatus="count1">
<tr>
	<td><c:out value="${bean.customerId}"></c:out></td>
	<td><c:out value="${bean.firstName}"></c:out></td>
	<td><c:out value="${bean.lastName}"></c:out></td>
	<td><c:out value="${bean.dateOfBirth}"></c:out></td>
	<td><c:out value="${bean.gender}"></c:out></td>
	<td><c:out value="${bean.phone}"></c:out></td>
	<td><c:out value="${bean.email}"></c:out></td>
	<td><c:out value="${bean.address}"></c:out></td>
	<td><c:out value="${bean.pancardNumber}"></c:out></td>
</tr>
</c:forEach>

</table>
</c:if>
<c:if test="${empty customerlist}">
		<h2>Your details are not available. Please try again later!</h2>
	</c:if>
</body>
</html>